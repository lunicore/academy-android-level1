package se.lunicore.academy.android.level1;

import android.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;


public class StartActivity extends Activity implements StartFragment.startActivityListener, DialogFragmentToast.DialogFragmentListener{

    Fragment childFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        if (savedInstanceState == null) {
            childFragment = new StartFragment();
            getFragmentManager().beginTransaction().add(R.id.container, childFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }


    @Override
    public void startSecondActivity() {
        Intent intent = new Intent(this, SecondActivity.class);
        Bundle extras = new Bundle();
        intent.putExtras(extras);
        startActivity(intent);
    }

    @Override
    public void showDialog() {
        DialogFragment newToastDialog = new DialogFragmentToast("My fantastic msg");
        newToastDialog.show(getFragmentManager(), "toastDialog");
    }

    @Override
    public void ToastME(String toast) {
        Toast.makeText(this, toast, Toast.LENGTH_LONG).show();
    }
}
