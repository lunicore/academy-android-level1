package se.lunicore.academy.android.level1;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by elias on 06/05/14.
 */
public class StartFragment extends Fragment implements View.OnClickListener{
    ImageView imageView;
    startActivityListener mListner;

    public StartFragment(){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListner = (startActivityListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement startActivityListener");
        }
    }
    public interface startActivityListener{
        public void startSecondActivity();
        public void showDialog();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate view from XML
        View rootView = inflater.inflate(R.layout.fragment_start, container, false);
        // Find XML objects and map them to java objects
        imageView = (ImageView) rootView.findViewById(R.id.fragment_start_imageView_arrow);
        Button XMLButton = (Button) rootView.findViewById(R.id.fragment_start_button_xml);
        Button startActivity = (Button) rootView.findViewById(R.id.fragment_start_button_new_activity);

        // Add onClickListener
        XMLButton.setOnClickListener(this);
        startActivity.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        imageView.setRotation(45);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.fragment_start_button_new_activity:
                mListner.startSecondActivity();
                break;
            case R.id.fragment_start_button_xml:
                mListner.showDialog();
                break;
        }
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @return A float value to represent px equivalent to dp depending on device density
     */
    private float convertDpToPixel(float dp){
        Resources resources = this.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
}
